var button = document.getElementById("button")
button.addEventListener("click", function getResult() {

    var first = document.getElementById("first").value
    var second = document.getElementById("second").value

    if (first != "" && second != "") {
        var url = "https://sumresolver.herokuapp.com/sum?first=" + first + "&second=" + second

        var req = new XMLHttpRequest()
        req.open("GET", url, false)
        req.send(null)
        
        if (req.status == 200 && req.readyState == 4) {
            var res = JSON.parse(req.responseText)
            window.alert("Il risultato è " + res.sum)
        } else {
            window.alert("Errore")
        }
       
    } else {
        window.alert("Non hai inserito uno dei due operandi!!")
    }
})