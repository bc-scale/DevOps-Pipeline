var path = require("path")
var express = require("express")

var app = express()

app.get("/", function(req, res) {
    res.sendFile(path.join(__dirname, "./dist/index.html"))
})
app.use(express.static("dist"))

var server = app.listen(process.env.PORT || 3000, function() {
    console.log("listening on port ", server.address().port)
})