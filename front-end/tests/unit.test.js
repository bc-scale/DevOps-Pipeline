const assert = require("assert")
const jsdom = require("jsdom")

const {JSDOM} = jsdom
const {document} = (new JSDOM("<!doctype html><html><body></body></html>")).window 

test("UNIT TEST", () => {
    const button = document.createElement("button")
    const first = document.createElement("input")
    const second = document.createElement("input")

    first.setAttribute("value", 0)
    second.setAttribute("value", 0)

    button.addEventListener("click", function getResult() {
        first.setAttribute("value", 2)
        second.setAttribute("value", 3)
    })

    button.click()
    assert.deepEqual(first.value, 2)
    assert.deepEqual(second.value, 3)
})