# DevOps Pipeline

## Descrizione
In questo progetto ho creato una pipeline di CI/CD in GitLab nel caso di un progetto scritto in Node.js.
Ci sono 7 fasi da dover completare prima che una modifica del codice venga effettivamente rilasciata e sia disponibile per tutti. In ognuno degli stage dobbiamo utilizzare diversi framework dedicati ad uno specifico task.
Il progetto consiste in un'applicazione Font-End che mostra un form in cui è possibile inserire 2 numeri ed inviarli ad una API Back-End. Il collegamento avviene tramite uno script che esegue una HTTPRequest GET.

## Stage
- **Build** — installa le dipendenze necessarie per il progetto
- **Verify** — analizza sintatticamente il codice tramite delle regole
- **Unit Test** — testa il corretto funzionamento della componente singola di Front-End
- **Integration Test** — testa il corretto collegamento tra le 2 componenti
- **Package** — crea un pacchetto di asset utilizzabile direttamente nel browser come folder dist
- **Release** — incrementa la versione di progetto e crea il tag relativo nel repository
- **Deploy** — aggiorna il codice in produzione rendendolo disponibile tramite link

## Script
Start app: ```npm start```

Dev server: ```npm run dev```

Build: ```npm run build```

Lint: ```npm run verify```


## FrameWork Utilizzati
In questo progetto ho utilizzato Node.js come linguaggio e npm come gestore dei pacchetti per javascript.
Per installare come dipendenze questi framework basta lanciare il comando **npm install nome_dipendenza --save-dev**.
I framework utilizzati per le varie fasi sono:

##### ESLint
ESLint è uno strumento di analisi del codice statico per identificare i modelli problematici presenti nel codice JavaScript. Utilizza delle regole per valutare la correttezza sintattica che possono essere inserite nel file di configurazione *.eslintrc.json*. Nello script presente in *package.json* chiamato verify è presente il comndo per eseguire ESLint ovvero **eslint ./**, basterà quindi eseguire **npm run verify**.

##### Flow
Flow è un controllore statico di tipo per il codice JavaScript. Permette di verificare se c'è un errore tra tipo di dato passato alla funzione e tipo di dato che la funzione definisce come parametro. Anche qui abbiamo un file di configurazione .flowconfig. Per lanciar questo sftware basta eseguire il comando **flow** sempre presente come comando sequenziale oltre ad ESLint in **npm run verify**

##### Jest
Jest è un framework di test JavaScript. È estato utilizzato per il testing in generale attraverso la keyword **test** come si vede qui sotto. È importante ricordare che i file di test devono contenere il nome *test*, una buona prassi e quella di rinominare il file in questo modo *nome.test.js*. Infine il comando per testare uno specifico file con jest è **jest --verbose percorso_al_file**
```javascript
test("NOME TEST", () => {
    //codice
})
```

##### JSDOM
JSDOM è una libreria che analizza e interagisce con l'HTML assemblato proprio come un browser. Il vantaggio è che non è un vero e proprio browser. Al contrario, implementa gli standard web come fanno i browser. È possibile dargli in pasto dell'HTML, che verrà analizzato. Quindi, è possibile ispezionare o modificare l'HTML in memoria utilizzando le normali API DOM di JavaScript.
Ho usato JSDOM per testare il funzionamento di AddEventListener che permette la gestione di un submit button con una funzione dedicata. Ho testato il click del bottone per verificare che la funzione venisse eseguita.

Qui sotto un esempio di come utilizzarlo:
```javascript
const jsdom = require("jsdom")
const {JSDOM} = jsdom
const {document} = (new JSDOM("<!doctype html><html><body></body></html>")).window

const button = document.createElement("button")
```

##### Webpack
Webpack è un bundler di moduli gratuito e open source per JavaScript. È realizzato principalmente per JavaScript, ma può trasformare risorse front-end come HTML, CSS e immagini se sono inclusi i caricatori corrispondenti. Webpack accetta moduli con dipendenze e genera asset statici che rappresentano tali moduli.
Permette di creare una cartella dist con all'interno file scritti su una sola riga per migliorare le performance oppure sostituire le chiamate a funzione con il corpo effettivo, inoltre permette di collegare HTML con file JS esterni tramite script.

È stato usato anche HtmlWebpackPlugin per i file HTML e webpack-dev-server per l'utilizzo di un server in development lanciato con il comando **npm run dev**. Il tutto è specificato in un file di configurazione chiamato *webpack.config.js*.

##### Express
Express.js, o semplicemente Express, è un framework per applicazioni web per Node.js. È stato progettato per creare web application e API ed ormai definito il server framework standard de facto per Node.js.
Ho usato questo framework per creare il file *server.js* che sarà utile in fese di deploy. Per eseguire il server e quindi l'applicativo il comando è **npm start** che in realtà nasconde **node server.js**.

##### Docker
Docker è software progettato per eseguire processi informatici in ambienti isolabili, minimali e facilmente distribuibili chiamati container Linux, con l'obiettivo di semplificare i processi di deployment di applicazioni software. In questo caso esiste un *Dockerfile* che specifica l'immagine in un container. L'immagine docker è una lista di istruzioni utili per eseguire l'applicazione necessarie al container: ad esempio le dipendenze, le configurazioni e tutti i file necessari. Il container è un processo linux isolato dalla macchina di host che lo ospita che viene creato seguendo le direttive dell'immagine docker.

- FROM — indica il linguaggio e il tipo di immagine
- WORKDIR — specifica la cartella di root del progetto
- COPY — copia all'interno della root directory un file
- RUN — esegue un comando prima di avviare l'applicativo
- ADD — aggiunge tutti i file in una specifica cartella destinazione
- CMD — indica il comando per avviare l'applicativo

Esempio struttura cartelle del progetto — *la cartella dist è creata con l'esecuzione di **npm run build***
```
front-end
│   package.json
│   package-lock.json
│   server.js
│   webpack.config.js
│
└───src
│   │   index.html
│   │   index.js
│   
└───dist
    │   index.html
    │   bundle.js
```

*Dockerfile* di esempio:
```Dockerfile
FROM node:18.12.0-buster-slim

WORKDIR /front-end
RUN mkdir src
COPY package.json .
COPY package-lock.json .
COPY src/index.html ./src
COPY src/index.js ./src
COPY server.js .
COPY webpack.config.js .

RUN npm install
RUN npm run build

ADD . .

CMD [ "npm", "start" ]
```

##### Heroku
Heroku è una platform as a service sul cloud che supporta diversi linguaggi di programmazione. Mi è servità per la fase di deploy del codice utilizzando il suo container registry. In questo caso faccio la push dell'immagine docker sul container registry e successivamente heroku mi fornirà un link dove potrò sempre usare la mia app.





